<?php
session_start();
if (!isset($_SESSION['usuario'])) {
    echo '<script>window.history.back();</script>';
    echo '<script>window.alert("Revise su clave");</script>';
    }
?>
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="img/colegioicon.png" type="image/png">

    <title>CSJ</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap.css" rel="stylesheet">
    <link href="css/diseno.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    
  </head>
  <body>

    <header>
      <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark"> 
      <img class="encabezado" src="img/colegio texto.png" >
      <img class="colegio" src="img/School-PNG-File.png" >
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarCollapse">
          <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
            <a class="navbar-brand" href="dashboard.php" >Inicio</a>
          <a class="navbar-brand" href="alumnos.php">Alumnos</a>
          <a class="navbar-brand" href="maestros.php"target="top">Maestros</a>
          <a class="navbar-brand" href="administrador.php">Administrador</a>
            </li>
            </ul>
           <?php
                //session_start();//inicia la sesion o la recupera
                if (isset($_SESSION['contrasena'])&&($_SESSION['usuario'])) {
                    echo '<a style="float: right"; class="nav-link disabled" href="#"> ' . $_SESSION['usuario'].' </a>';
                    echo '<script>window.location.href = "dashboard.php"();</script>';
                }  
            ?> 
            
        </div>
        <li><a href="cerrarAcceso.php">Salir</a></li>
          
       
                   
      </nav>
    </header>
    
    <main role="main">
       <div class="container">
        <hr class="featurette-divider">
        <img class="fondopizzarra" src="img/fondopizarra.jpg" >
        </div>
        
<!-- .social-sharing -->
     
  <aside class="social-sharing">
  <th class="social-item"><a href="http://www.twwhatsapp://send"><img class="redes" src="img/whatsapp.png" > </a></th>
            <th class="social-item "><a href="http://www.twitter.com"><img class="redes" src="img/twitter1.png" > </a></th>
            <th class="social-item "><a href="http://www.pinterest.com"> <img class="redes" src="img/pinterest.png" ></a></th>
            <th class="social-item "><a href="http://www.facebook.com"><img class="redes" src="img/facebook2.jpg" > </a></th>
           
    </aside>
       <!-- FOOTER -->
      <div >
      <footer class="footero">
       <img class="footero" src="img/footer.png" >
       
       </footer>
       
       </div>
        <a href ="controllervista/cursosm.php"><img class="imagen1" src="img/educacion (1).png" ></a>
        <a href ="controllervista/horariosm"><img class="imagen2" src="img/empleado.png" >
        <a href ="controllervista/alumnosm"><img class="imagen3" src="img/graduarse.png" >
        <a href ="controllervista/notasm"><img class="imagen4" src="img/boletin-de-calificaciones.png" >
        <p class= "texto1">Cursos</p>
        <p class= "texto2">Horarios</p>
        <p class= "texto3">Alumnos</p>
        <p class= "texto4">Notas</p>
    </main>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script>window.jQuery || document.write('<script src="js/vendor/jquery-slim.min.js"><\/script>')</script>
    <script src="js/vendor/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <!-- Just to make our placeholder images work. Don't actually copy the next line! -->
    <script src="js/vendor/holder.min.js"></script>
    
      <div id="left"></div>
      <div id="right"></div>
      <div id="top"></div>
      <div id="bottom"></div>
  </body>
</html>