<?php
session_start();
if (!isset($_SESSION['usuario'])) {
    echo '<script>window.history.back();</script>';
    echo '<script>window.alert("Revise su clave");</script>';
    }
?>
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="http://www.walmart.com.gt/wp-content/uploads/2015/07/favicon.png" type="image/png">

    <title>CSJ</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap.css" rel="stylesheet">
    <link href="css/diseno.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    
  </head>
  <body>

    <header>
      <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark"> 
      <img class="encabezado" src="img/colegio texto.png" >
      <img class="colegio" src="img/School-PNG-File.png" >
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarCollapse">
          <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
            <a class="navbar-brand" href="dashboard.php" target="top">Inicio</a>
          <a class="navbar-brand" href="alumnos.php">Alumnos</a>
          <a class="navbar-brand" href="maestros.php">Maestros</a>
          <a class="navbar-brand" href="administrador.php">Administrador</a>
            </li>
            </ul>
           <?php
                //session_start();//inicia la sesion o la recupera
                if (isset($_SESSION['contrasena'])&&($_SESSION['usuario'])) {
                    echo '<a style="float: right"; class="nav-link disabled" href="#"> ' . $_SESSION['usuario'].' </a>';
                    echo '<script>window.location.href = "dashboard.php"();</script>';
                }  
            ?> 
            
        </div>
        <li><a href="cerrarAcceso.php">Salir</a></li>
          
       
                   
      </nav>
    </header>

    <main role="main">
       <div class="container">
        <hr class="featurette-divider">
        <img class="fondopizzarra" src="img/fondopizarra.jpg" >
        </div>
        <img class="quienes1" src="img/colegio texto.png" >
      <img class="quienes2" src="img/School-PNG-File.png" >
<!-- .social-sharing -->
     
  <aside class="social-sharing">
        
        <li class="social-item"><a href="http://www.twwhatsapp://send"><img class="redes" src="img/whatsapp.png" > </a></li>
            <li class="social-item "><a href="http://www.twitter.com"><img class="redes" src="img/twitter1.png" > </a></li>
            <li class="social-item "><a href="http://www.pinterest.com"> <img class="redes" src="img/pinterest.png" ></a></li>
            <li class="social-item "><a href="http://www.facebook.com"><img class="redes" src="img/facebook2.jpg" > </a></li>
           
    </aside>
       <!-- FOOTER -->
      <div >
      <footer class="footero">
       <img class="footero" src="img/footer.png" >
       
       </footer>
       
       </div>
    </main>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script>window.jQuery || document.write('<script src="js/vendor/jquery-slim.min.js"><\/script>')</script>
    <script src="js/vendor/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <!-- Just to make our placeholder images work. Don't actually copy the next line! -->
    <script src="js/vendor/holder.min.js"></script>
    
      <div id="left"></div>
      <div id="right"></div>
      <div id="top"></div>
      <div id="bottom"></div>
  </body>
</html>
