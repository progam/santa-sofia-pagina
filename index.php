
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="img/colegioicon.png" type="image/png">

    <title>CSJ</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap.css" rel="stylesheet">
    <link href="css/diseno.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    
  </head>
  <body>

    <header>
      <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark"> 
      <img class="encabezado" src="img/colegio texto.png" >
      <img class="colegio" src="img/School-PNG-File.png" >
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarCollapse">
          <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
            <a class="navbar-brand" href="index.php" target="top">Inicio</a>
          <a class="navbar-brand" href="quienessomos.php">Quienes Somos</a>
          <a class="navbar-brand" href="contacto.php">contactos</a>
            </li>
            </ul>
            
            
        </div> 
        
        <?php
               session_start();//inicia la sesion o la recupera
                if (isset($_SESSION['contrasena'])&&($_SESSION['usuario'])) {
                    echo '<a style="float: right"; class="nav-link disabled" href="#"> ' . $_SESSION['usuario'].' </a>';
                    echo '<script>window.location.href = "dashboard.php"();</script>';
                }  else {
                    echo'<form class="form-inline mt-2 mt-md-0" action="acceso.php" method="post">
                            <input name="usuario" class="form-control mr-sm-2" type="email" id="inputEmail"  placeholder="Usuario" aria-label="usuario" >
                            <input name="contrasena" class="form-control mr-sm-2" type="password" id="inputPassword" placeholder="Contraseña" aria-label="contraseña" >
                            <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Ingresar</button>
                           <a <button class="btn btn-outline-primary my-2 my-sm-2" href="registrar.php">Registrar</button></a>
                            
                        </form>';
                }
            ?>     
      </nav>
    </header>

    <main role="main">
       <div class="container">
        <hr class="featurette-divider">
        <img class="fondopizzarra" src="img/fondopizarra.jpg" >
        </div>
        <img class="quienes1" src="img/colegio texto.png" >
      <img class="quienes2" src="img/School-PNG-File.png" >
<!-- .social-sharing -->
     
  <aside class="social-sharing">
        
        <th class="social-item"><a href="http://www.twwhatsapp://send"><img class="redes" src="img/whatsapp.png" > </a></th>
            <th class="social-item "><a href="http://www.twitter.com"><img class="redes" src="img/twitter1.png" > </a></th>
            <th class="social-item "><a href="http://www.pinterest.com"> <img class="redes" src="img/pinterest.png" ></a></th>
            <th class="social-item "><a href="http://www.facebook.com"><img class="redes" src="img/facebook2.jpg" > </a></th>
           
    </aside>
       <!-- FOOTER -->
      <div >
      <footer class="footero">
       <img class="footero" src="img/footer.png" >
       
       </footer>
       
       </div>
    </main>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script>window.jQuery || document.write('<script src="js/vendor/jquery-slim.min.js"><\/script>')</script>
    <script src="js/vendor/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <!-- Just to make our placeholder images work. Don't actually copy the next line! -->
    <script src="js/vendor/holder.min.js"></script>
    
      <div id="left"></div>
      <div id="right"></div>
      <div id="top"></div>
      <div id="bottom"></div>
  </body>
</html>